<?php

namespace Merlin\ConsoleProcessManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Mateusz Krysztofiak <m.krysztofiak@topmall.pl>
 */
class ConsoleProcessManagerBundle extends Bundle
{
}
