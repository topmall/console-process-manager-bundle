<?php

namespace Merlin\ConsoleProcessManagerBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Merlin\ConsoleProcessManagerBundle\Entity\Call;
use Merlin\ConsoleProcessManagerBundle\Entity\Process;

/**
 * CallRepository
 *
 * @author Mateusz Krysztofiak <m.krysztofiak@topmall.pl>
 */
class CallRepository extends EntityRepository
{
    public static $erName = 'ConsoleProcessManagerBundle:Call';

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create($this->_em->getConnection(), $this->_em->getConfiguration());
        }

        return $this->_em;
    }

    public function checkEntityManager()
    {
        $this->getEntityManager();
    }

    /**
     * @param Call $call
     * @return Call
     */
    public function update(Call $call) {

        $em = $this->getEntityManager();
        $em->persist($call);
        $em->flush($call);

        return $call;
    }

    /**
     * @param Process $process
     * @param null $status
     * @param null $inLastHours
     * @param bool $autoUpdateProcess
     * @return mixed
     * @internal param $processId
     */
    public function countByProcessIdAndStatus(Process $process, $status = null, $inLastHours = null, $autoUpdateProcess = false)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder('pmc')
            ->from(self::$erName, 'pmc')
            ->select('count(pmc)')
            ->where('pmc.process = :id')
            ->setParameter('id', $process->getId());

        if($status) {
            if(is_array($status)) {
                $qb->andWhere('pmc.status IN(:status)');
            } else {
                $qb->andWhere('pmc.status = :status');
            }
            $qb->setParameter('status', $status);
        }

        if($inLastHours) {
            $now = new \DateTime();
            $qb->andWhere('pmc.finishedAt > :inLastHours')
                ->setParameter('inLastHours', $now->sub(new \DateInterval('PT' . $inLastHours . 'H')));
        }

        $result = $qb->getQuery()->getOneOrNullResult();
        $forReturn = $result && is_array($result) ? (int)$result[1] : 0;

        if($autoUpdateProcess) {
            $process->setCallErrorCount($forReturn);
            $em = $this->getEntityManager();
            $em->persist($process);
            $em->flush($process);
        }

        return $forReturn;
    }

    /**
     * @param int $time
     * @param int $limit
     * @param int $server
     * @return array
     */
    public function getPidsInProgressWithLongTimeExecution($time = 180, $limit = 100, $server = Process::SERVER_ERP)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $now = new \DateTime();
        $q  = $qb->select(['pmc.consolePid', 'pmc.id'])
            ->from(self::$erName, 'pmc')
            ->innerJoin('pmc.process', 'p')
            ->where('pmc.createdAt < :time')
            ->andWhere('pmc.status = :status')
            ->andWhere('p.server = :server')
            ->setParameter('time', $now->sub(new \DateInterval('PT' . $time . 'M')))
            ->setParameter('status', Call::STATUS_STARTED)
            ->setParameter('server', $server)
            ->setMaxResults($limit)
            ->getQuery();

        return $q->getResult();
    }

    /**
     * @param $status
     * @param $ids
     * @return int
     */
    public function updateStatusByIds($status, $ids)
    {
        if (empty($ids)) {
            return true;
        }

        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        return $conn->executeQuery('update console_manager_call set status = ?, finished_at = now() where id in (?)',
            [$status, $ids],
            [\PDO::PARAM_STR, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
        );
    }

    /**
     * @return int
     */
    public function deleteOldCalls()
    {
        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        return $conn->executeUpdate('
            DELETE FROM console_manager_call
            WHERE created_at < DATE_SUB(curdate(), INTERVAL 2 WEEK);
        ');
    }

    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @param $process
     * @param null $status
     * @param int $server
     * @return Call[]
     */
    public function findCalls($process, $status = null, $server = Process::SERVER_ERP)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->innerJoin('c.process', 'p');
        $qb->andWhere($qb->expr()->eq('c.process', $process->getId()));
        $qb->andWhere($qb->expr()->eq('p.server', $server));
        $qb->andWhere($qb->expr()->eq('c.status', $status));

        return $qb->getQuery()->getResult();
    }
}
