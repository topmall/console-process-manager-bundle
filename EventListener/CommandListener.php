<?php
namespace Merlin\ConsoleProcessManagerBundle\EventListener;

use Exception;
use Merlin\ConsoleProcessManagerBundle\Entity\Call;
use Merlin\ConsoleProcessManagerBundle\Entity\Process;
use Merlin\ConsoleProcessManagerBundle\Repository\CallRepository;
use Merlin\ConsoleProcessManagerBundle\Repository\ProcessRepository;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;

/**
 * @author Mateusz Krysztofiak <m.krysztofiak@topmall.pl>
 */
class CommandListener
{
    /**
     * @var ProcessRepository
     */
    private $processRepository;

    /**
     * @var CallRepository
     */
    private $callRepository;

    private $params;

    /**
     * @param mixed $processRepository
     * @return CommandListener
     */
    public function setProcessRepository(ProcessRepository $processRepository)
    {
        $this->processRepository = $processRepository;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @param mixed $callRepository
     * @return CommandListener
     */
    public function setCallRepository(CallRepository $callRepository)
    {
        $this->callRepository = $callRepository;
    }

    public function isEnabled()
    {
        return isset($_SERVER['argv']) && $this->params['enabled'];
    }

    /**
     * Run on console command start
     *
     * @param ConsoleCommandEvent $event
     * @return bool
     * @throws Exception
     */
    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        $blacklist = [
            'doctrine:schema:drop',
            'doctrine:schema:update',
            'doctrine:schema:create',
            'doctrine:fixtures:load',
            'doctrine:migrations:migrate',
            'doctrine:migrations:diff',
            'doctrine:schema:validate',
            'cache:clear',
            'assets:install',
            'assetic:dump',
            'server:run',
            'app:generate-locations',
            'app:generate-workstations',
            'sonata:admin:generate',
            'bernard:consume',
            'rabbitmq:consumer',
        ];

        if (in_array($event->getCommand()->getName(), $blacklist)) {
            return true;
        }

        try {
            if ($this->isEnabled()) {
                $argv = $_SERVER['argv'];
                unset($argv[0]);
                $server = ('befpm' == gethostname()) ? Process::SERVER_FPM :  Process::SERVER_ERP;
                $command = implode(' ', $argv);

                if (!$process = $this->processRepository->findCommand($command, $server)) {
                    $commandName = $event->getCommand()->getName();
                    $process = $this->processRepository->createProcess($commandName, $command, $server);
                }

                if ($this->preventDuplicate($process, $event, $server)) {
                    return true;
                }

                $call = new Call();
                $call->setStatus(Call::STATUS_STARTED);
                $call->setConsolePid($this->getPid());
                $this->processRepository->addCallToProcess($process, $call);
                $process->setLastCall($call);
                $this->processRepository->update($process);

                $_REQUEST['call_id'] = $call->getId();
            }
        } catch (\Exception $e) {
            $this->execErrorToSentry($e);
        }

        return true;
    }

    /**
     * @param Process $process
     * @param ConsoleCommandEvent $event
     * @param $server
     * @return bool
     */
    private function preventDuplicate(Process $process, ConsoleCommandEvent $event, $server = Process::SERVER_ERP)
    {
        if ($process->getCommandName() == 'console-process-manager:process-call-clean-up') {
            $skip = false;
        }

        if (!isset($skip)) {
            $inProcessCall = $this->callRepository->findCalls($process, Call::STATUS_STARTED, $server);
            $skip = !empty($inProcessCall);
        }

        if ($skip) {
            $event->disableCommand();
            $_REQUEST['call_id'] = false;

            return true;
        }

        return false;
    }

    /**
     * Run on console command exception
     *
     * @param ConsoleExceptionEvent $event
     */
    public function onConsoleException(ConsoleExceptionEvent $event)
    {
        try {
            if (isset($_REQUEST['call_id']) && false !== $_REQUEST['call_id']) {
                $call = $this->callRepository->find($_REQUEST['call_id']);

                $call->setStatus(Call::STATUS_FAILED)
                    ->setOutput($event->getException());

                $this->callRepository->update($call);

                $this->registerError($call);
            }
        } catch (\Exception $e) {
            $this->execErrorToSentry($e);
        }
    }

    /**
     * Run on console command ends
     *
     * @param ConsoleTerminateEvent $event
     */
    public function onConsoleTerminate(ConsoleTerminateEvent $event)
    {
        try {
            if (isset($_REQUEST['call_id']) && false !== $_REQUEST['call_id']) {
                $this->callRepository->checkEntityManager();
                $call = $this->callRepository->find($_REQUEST['call_id']);

                if (!$call->getStatus()) {
                    $call->setStatus(Call::STATUS_SUCCESS);
                }

                if ($event->getExitCode()) {
                    $call->setStatus(Call::STATUS_ABORTED);
                    $call->setOutput(sprintf('Command finished with exit code: %s', $event->getExitCode()));

                    $this->registerError($call);
                }

                $call->setFinishedAt(new \DateTime());
                $call->setExecutionTime($call->getExecutionTime());
                $this->callRepository->update($call);

                $this->processRepository->countAvgExecutionTime($call->getProcess(), $call->getExecutionTime());
                $this->processRepository->update($call->getProcess());
            }
        } catch (\Exception $e) {
            $this->execErrorToSentry($e);
        }
    }

    /**
     * Returns console command line process ID
     *
     * @return int
     */
    private function getPid()
    {
        return posix_getpid();
    }

    /**
     * @param Call $call
     */
    private function registerError(Call $call)
    {
        try {
            $process = $call->getProcess();
            $process->setCallLastErrorTime(new \DateTime());

            $this->processRepository->update($process);
        } catch (\Exception $e) {
            $this->execErrorToSentry($e);
        }
    }

    private function execErrorToSentry(\Exception $e)
    {
        \trigger_error(\implode('|', [$e->getMessage(), $e->getFile(), $e->getLine()]));
    }
}
