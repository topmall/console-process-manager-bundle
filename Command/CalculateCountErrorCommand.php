<?php
namespace Merlin\ConsoleProcessManagerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateCountErrorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('console-process-manager:calculate-count-error');
        $this->setDescription('Calculate count error');
    }

    protected function execute(InputInterface $in, OutputInterface $out)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $processRepository = $em->getRepository('ConsoleProcessManagerBundle:Process');
        $lastFailedCallCounters = $processRepository->updateErrorCounter();
        $out->writeln('[CALC COUNT ERROR] Last updated ' . count($lastFailedCallCounters) . ' processes');
    }
}