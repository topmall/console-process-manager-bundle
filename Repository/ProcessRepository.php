<?php

namespace Merlin\ConsoleProcessManagerBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
//use Doctrine\ORM\Tools\SchemaTool;
use Merlin\ConsoleProcessManagerBundle\Entity\Call;
use Merlin\ConsoleProcessManagerBundle\Entity\Process;
//use Symfony\Component\Console\Input\ArgvInput;

/**
 * ProcessRepository
 *
 * @author Mateusz Krysztofiak <m.krysztofiak@topmall.pl>
 */
class ProcessRepository extends EntityRepository
{
    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create($this->_em->getConnection(), $this->_em->getConfiguration());
        }

        return $this->_em;
    }

    public function checkEntityManager()
    {
        $this->getEntityManager();
    }

    /**
     * @param $commandName
     * @param $command
     * @param $server
     * @return Process
     */
    public function createProcess($commandName, $command, $server = Process::SERVER_ERP)
    {
        $process = new Process();
        $process->setCommandName($commandName)
            ->setCommand($command)
            ->setExecutionCounter(0)
            ->setAvgExecutionTime(0);
        $process->setServer($server);

        $em = $this->getEntityManager();
        $em->persist($process);
        $em->flush();

        return $process;
    }

    /**
     * @param Process $process
     * @return Process
     */
    public function update(Process $process)
    {
        $em = $this->getEntityManager();
        $em->persist($process);
        $em->flush($process);

        return $process;
    }

    /**
     * @param Process $process
     * @param Call $call
     * @return Process
     */
    public function addCallToProcess(Process $process, Call $call)
    {
        $call->setProcess($process);

        $process = $this->increaseExecutionCounter($process);

        $em = $this->getEntitymanager();
        $em->persist($call);
        $em->flush();

        $process->addCall($call);

        return $process;
    }

    /**
     * @param Process $process
     * @return Process
     */
    public function increaseExecutionCounter(Process $process)
    {
        return $process->setExecutionCounter($process->getExecutionCounter() + 1);
    }

    /**
     * @param Process $process
     * @param $newExecutionTime
     * @return Process
     */
    public function countAvgExecutionTime(Process $process, $newExecutionTime)
    {
        if (!$process->getAvgExecutionTime()) {
            return $process->setAvgExecutionTime($newExecutionTime);
        } else {
            return $process->setAvgExecutionTime(
                (($process->getExecutionCounter() - 1) * $process->getAvgExecutionTime() + $newExecutionTime)
                / $process->getExecutionCounter()
            );
        }
    }

    /**
     * @param int $inLastHours
     * @return array
     */
    public function updateErrorCounter($inLastHours = 72) {
        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        $lastFailedCallCounters = $conn->executeQuery('
            SELECT
              cmc.process_id  AS processId,
              sum(
                  CASE WHEN
                    cmc.finished_at BETWEEN DATE_SUB(NOW(), INTERVAL 72 HOUR) AND NOW()
                    THEN 1
                  ELSE 0 END) AS callErrorCount,
              cmp.call_error_count
            FROM
              console_manager_call cmc
              JOIN console_manager_process cmp ON cmp.id = cmc.process_id
            WHERE
              cmc.status IN (2, 3)
            GROUP BY cmc.process_id
            HAVING callErrorCount != cmp.call_error_count;
        ')->fetchAll();

        foreach ($lastFailedCallCounters as $c) {
            $stmt = $em->getConnection()->prepare("
                UPDATE console_manager_process
                SET call_error_count = :callErrorCount WHERE id = :processId;"
            );
            $stmt->bindValue('callErrorCount', $c['callErrorCount']);
            $stmt->bindValue('processId', $c['processId']);
            $stmt->execute();
        }

        return $lastFailedCallCounters;
    }

    /**
     * @return int
     */
    public function deleteProcessesWithoutLastCall()
    {
        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        return $conn->executeUpdate('
            DELETE FROM console_manager_process
            WHERE
              NOT exists(
                  SELECT c.id
                  FROM console_manager_call c
                  WHERE last_call_id = c.id
              )
              OR last_call_id IS NULL;
        ');
    }

    /**
     * @param $command
     * @param int $server
     * @return Process
     */
    public function findCommand($command, $server = Process::SERVER_ERP)
    {
        return $this->findOneBy(['command' => $command, 'server' => $server], ['id' => 'desc']);
    }
}
