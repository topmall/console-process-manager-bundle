<?php
namespace Merlin\ConsoleProcessManagerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Merlin\ConsoleProcessManagerBundle\Repository\CallRepository;
use Merlin\ConsoleProcessManagerBundle\Repository\ProcessRepository;

class DeleteOldProcessesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('console-process-manager:delete-old-processes');
        $this->setDescription('Delete all processes and calls older than two weeks.');
    }

    protected function execute(InputInterface $in, OutputInterface $out)
    {
        sleep(10);
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var CallRepository $callRepository */
        $callRepository = $em->getRepository('ConsoleProcessManagerBundle:Call');
        /** @var ProcessRepository $processRepsitory */
        $processRepsitory = $em->getRepository('ConsoleProcessManagerBundle:Process');
        $out->writeln(sprintf('%d calls older than 2 week were deleted', $callRepository->deleteOldCalls()));
        $out->writeln(sprintf('%d processes without last call were deleted', $processRepsitory->deleteProcessesWithoutLastCall()));
    }
}