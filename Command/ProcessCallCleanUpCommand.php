<?php
namespace Merlin\ConsoleProcessManagerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Merlin\ConsoleProcessManagerBundle\Entity\Call;
use Merlin\ConsoleProcessManagerBundle\Entity\Process;
use Merlin\ConsoleProcessManagerBundle\Repository\CallRepository;
use Merlin\ConsoleProcessManagerBundle\Repository\ProcessRepository;

class ProcessCallCleanUpCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('console-process-manager:process-call-clean-up');
        $this->setDescription('Find all calls in progress status and time execution long time and check if pid exists in system processes. If not change status to finished.');
        $this->addOption('time', null, InputOption::VALUE_REQUIRED, 'Max runtime criteria to check calls.', 180);
        $this->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Limit for processing calls. (sql limit)', 1000);
        $this->addOption('server', null, InputOption::VALUE_OPTIONAL, 'Counting errors for all processes.', 0);
        $this->addOption('count-errors', null, InputOption::VALUE_NONE, 'Counting errors for all processes.');
    }

    protected function execute(InputInterface $in, OutputInterface $out)
    {
        $time = $in->getOption('time');
        $limit = $in->getOption('limit');
        $server = ('befpm' == gethostname()) ? Process::SERVER_FPM :  Process::SERVER_ERP;

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var CallRepository $callRepository */
        $callRepository = $em->getRepository('ConsoleProcessManagerBundle:Call');
        $calls = $callRepository->getPidsInProgressWithLongTimeExecution($time, $limit, $server);

        $ids = [];
        foreach ($calls as $call) {
            if (!file_exists("/proc/{$call['consolePid']}")) {
                $ids[] = $call['id'];
            }
        }

        $callRepository->updateStatusByIds(Call::STATUS_ABORTED, $ids);
    }
}